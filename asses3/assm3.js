function initialise() {

	setupNavigation();
	setupTicker();
	setupStories();
	setupStoryTabs();
}

/* Part1: Navigation Bar */
function setupNavigation() {

	var navigate = document.getElementById("navigate");
	var menuitemsholders = navigate.getElementsByClassName("menuitemsholder");

	// hide sub-menu items.
	for (var i = 0; i < menuitemsholders.length; i++) {
		menuitemsholders[i].style.display = "none";
	};

	// display sub-menu on hover
	var menus = navigate.getElementsByClassName("menu");
	for (var i = 0; i < menus.length; i++) {
		menuHover(menus[i]);
	};	
}

function menuHover(menu) {
	
	menu.onmouseover = function() {
		var menuitemsholder = menu.getElementsByClassName("menuitemsholder")[0];
		if (menuitemsholder) {
			menuitemsholder.style.display = "block";
		};
	};
	
	menu.onmouseout = function() {
		var menuitemsholder = menu.getElementsByClassName("menuitemsholder")[0];
		if (menuitemsholder) {
			menuitemsholder.style.display = "none";
		};
	};
}

/***************************/

/* Part2: Ticker */
function setupTicker() {

	setInterval(updateTicker(), 3000);
}

function updateTicker() {

	var newslist = [];
	var newscounter = 0;

	var newstorylist = document.getElementById("newstories-list");
	var newstory = newstorylist.getElementsByTagName("span");

	for (var i = 0; i < newstory.length; i++) {
		newslist.push(newstory[i].innerHTML);
	};

	return function() {

		var breakingNews = document.getElementById("breaking");
		breakingNews.innerHTML = "<b>Breaking news:</b> " + newslist[newscounter]
		newscounter = (newscounter + 1) % newslist.length;
	}
}

/***************************/

/* Part3: Hide Stories */
function setupStories() {

	var storypanel = document.getElementById("storypanel");
	var stories = storypanel.getElementsByClassName("story");
	var storytitle = storypanel.getElementsByClassName("header");

	for (var i = 0; i < stories.length; i++) {
		stories[i].style.display = "none";
	};

	storytitle[0].innerHTML = stories[0].getElementsByClassName("title")[0].innerHTML;
	stories[0].getElementsByClassName("title")[0].style.display = "none";
	stories[0].style.display = "block";

	// show story if link clicked
	var storylists = document.getElementById("storylists");
	var storylist = storylists.getElementsByClassName("storylist");

	for (var i = 0; i < storylist.length; i++) {
		var links = storylist[i].getElementsByClassName("fakelink");
		for (var j = 0; j < links.length; j++) {
			var link = links[j].id;
			linkClick(link);
		};
	};
}

function linkClick(link) {

	document.getElementById(link).onclick = function() {
		var storypanel = document.getElementById("storypanel");
		var stories = storypanel.getElementsByClassName("story");
		var storytitle = storypanel.getElementsByClassName("header");
		for (var i = 0; i < stories.length; i++) {
			stories[i].style.display = "none";
		};

		var story = document.getElementById(link.split("-")[1]);
		storytitle[0].innerHTML = story.getElementsByClassName("title")[0].innerHTML;
		story.getElementsByClassName("title")[0].style.display = "none";
		story.style.display = "block";
	};
}

/***************************/

/* Part4: Story Tabs */
function setupStoryTabs() {

	// show story and change header color if clicked
	var listheaders = document.getElementById("listheaders").children;
	for (var i = 0; i < listheaders.length; i++) {
		var story = listheaders[i].id;
		storyClick(story);
	};
}

function storyClick(story) {
	document.getElementById(story).onclick = function() {
		var listheaders = document.getElementById("listheaders");
		var listheader = listheaders.getElementsByClassName("listheader")[0];
		listheader.className = "listheader-hidden";

		var storylists = document.getElementById("storylists");
		var storylist = storylists.getElementsByClassName("storylist");
		for (var i = 0; i < storylist.length; i++) {
			storylist[i].style.display = "none";
		};

		var storyheader = document.getElementById(story);
		storyheader.className = "listheader";

		var storylist = document.getElementById(story + "-list");
		storylist.style.display = "block";
	};
}

/***************************/