var topArtists = [];
var topTracks = [];
var events = [];

var artistSearch = [];
var trackSearch = [];
var eventSearch = [];

function initialise() {

    setupIntroPage();
    setupTopArtists();
    setupTopTracks();
    setupEvents();
    setupSearch();

    setupNavigation();
}

function setupIntroPage() {
    var introText = document.getElementById("charts");

    while (introText.childNodes.length > 0) {
        introText.removeChild(introText.firstChild);
    }

    var tr = document.createElement("tr");
    introText.appendChild(tr);

    var introHeading = document.createElement("th");
    introHeading.innerHTML = "Welcome to the world of Music!";
    tr.appendChild(introHeading);

    var trIntro = document.createElement("tr");
    trIntro.className = "introduction";
    introText.appendChild(trIntro);

    var p = document.createElement("p");
    p.innerHTML = "Listed below are the features of this project - <br />";
    trIntro.appendChild(p);

    var ul = document.createElement("ul");
    trIntro.appendChild(ul);

    var art = document.createElement("li");
    var trk = document.createElement("li");
    var evnt = document.createElement("li");
    var srch = document.createElement("li");

    art.innerHTML = "TOP ARTISTS page displays the extracted data in a well formated fashion. Links to artist page are available";
    trk.innerHTML = "TOP TRACKS page dispaly a list of tracks in a structured tabular format. You can click the headings of the table to sort its data.";
    evnt.innerHTML = "EVENTS page displays all the event data in a structured format connecting the venu site.";
    srch.innerHTML = "SEARCH bar allows searching all of the data on this page no matter which page we are currently on. It finds Artists and tracks related to them and also their events. Pressing enter doesn't work so please use the search button";

    ul.appendChild(art);
    ul.appendChild(trk);
    ul.appendChild(evnt);
    ul.appendChild(srch);

    var note = document.createElement("p");
    note.innerHTML = "NOTE: This discription is NOT part of the assessment. It is here to help who ever is marking this assignment.";
    trIntro.appendChild(note);
}

/*********************** NAVIGATION **********************/

function setupNavigation() {

    var navitems = document.getElementsByClassName("navInactive");
    for (var i = 0; i < navitems.length; i++) {
        navitems[i].onclick = function () {
            loadPage(this.id);
        };
    }
}

function loadPage(navID) {

    var navitems = document.getElementsByClassName("navActive");

    for (var i = 0; i < navitems.length; i++) {
        if (navitems[i].className == "navActive") {
            navitems[i].className = "navInactive";
        }
    }

    var navitems = document.getElementsByClassName("navInactive");

    for (var i = 0; i < navitems.length; i++) {
        if (navitems[i].id == navID) {
            navitems[i].className = "navActive";
        }
    }

    switch (navID) {
        case "artists":
            fillTopArtistsChart();
            break;
        case "tracks":
            fillTopTracksChart();
            break;
        case "events":
            fillEventsChart();
            break;
        default:
            console.log("Error: loading page failed");
    }
}

/*********************** ARTISTS *************************/

function setupTopArtists() {

    requestData("_assets/data/topartists.json", loadTopArtists);
}

function loadTopArtists(xmlhttp) {

    var jsonDoc = JSON.parse(xmlhttp.responseText);
    var jsonItems = jsonDoc.artists.artist;
    //console.log(jsonItems);

    for (var i = 0; i < jsonItems.length; i++) {

        var artistName = jsonItems[i].name;
        var artistImg = jsonItems[i].image[3]["#text"];
        var artistListeners = jsonItems[i].listeners;
        var artistPlaycount = jsonItems[i].playcount;
        var artistURL = jsonItems[i].url;

        var artist = {
            name: artistName,
            img: artistImg,
            listeners: artistListeners,
            playcount: artistPlaycount,
            URL: artistURL
        };

        topArtists.push(artist);
    }
    /*
    for(var i = 0; i < topArtists.length; i++) { 
    	console.log(topArtists[i]);
    }*/
}

function fillTopArtistsChart() {

    var topArtistsChart = document.getElementById("charts");

    while (topArtistsChart.childNodes.length > 0) {
        topArtistsChart.removeChild(topArtistsChart.firstChild);
    }

    for (var i = 0; i < topArtists.length; i++) {

        //Creating row
        var tr = document.createElement("tr");
        tr.className = "artist";
        topArtistsChart.appendChild(tr);

        var divImg = document.createElement("div");
        divImg.className = "artistImage";
        var img = document.createElement("img");
        img.setAttribute("src", topArtists[i].img);
        divImg.appendChild(img);
        tr.appendChild(divImg);

        var divName = document.createElement("div");
        divName.className = "artistName";
        var link = document.createElement("a");
        link.setAttribute("href", topArtists[i].URL);
        link.innerHTML = topArtists[i].name;
        divName.appendChild(link);
        tr.appendChild(divName);

        var divListeners = document.createElement("div");
        divListeners.className = "artistListeners";
        divListeners.innerHTML = topArtists[i].listeners;
        tr.appendChild(divListeners);

        var divCount = document.createElement("div");
        divCount.className = "artistPlaycount";
        divCount.innerHTML = topArtists[i].playcount;
        tr.appendChild(divCount);
    }
}
/************************ END ****************************/

/*********************** TRACKS **************************/

function setupTopTracks() {

    requestData("_assets/data/toptracks.json", loadTopTracks);
}

function loadTopTracks(xmlhttp) {

    var jsonDoc = JSON.parse(xmlhttp.responseText);
    var jsonItems = jsonDoc.tracks.track;
    //console.log(jsonItems);

    for (var i = 0; i < jsonItems.length; i++) {

        var trackName = jsonItems[i].name;
        var trackArtistName = jsonItems[i].artist.name;
        var trackListeners = jsonItems[i].listeners;
        var trackPlaycount = jsonItems[i].playcount;
        var trackURL = jsonItems[i].url
        var trackArtistURL = jsonItems[i].artist.url;

        var track = {
            name: trackName,
            artistName: trackArtistName,
            listeners: trackListeners,
            playcount: trackPlaycount,
            URL: trackURL,
            artistURL: trackArtistURL
        };

        topTracks.push(track);
    }
    /*
    for(var i = 0; i < topTracks.length; i++) { 
    	console.log(topTracks[i]);
    }*/
}

function fillTopTracksChart() {

    var topTracksChart = document.getElementById("charts");

    while (topTracksChart.childNodes.length > 0) {
        topTracksChart.removeChild(topTracksChart.firstChild);
    }

    //New row 
    var tr = document.createElement("tr");
    tr.className = "tracks";
    topTracksChart.appendChild(tr);

    //Columns Headings
    var thName = document.createElement("th");
    thName.innerHTML = "Tracks";
    thName.onclick = function () {
        sortByTracks();
    };
    tr.appendChild(thName);

    var thArtist = document.createElement("th");
    thArtist.innerHTML = "Artist";
    thArtist.onclick = function () {
        sortByArtists();
    };
    tr.appendChild(thArtist);

    var thListeners = document.createElement("th");
    thListeners.innerHTML = "Listeners";
    thListeners.onclick = function () {
        sortByListeners();
    };
    tr.appendChild(thListeners);

    var thCount = document.createElement("th");
    thCount.innerHTML = "Play Count";
    thCount.onclick = function () {
        sortByPlayCount();
    };
    tr.appendChild(thCount);

    for (var i = 0; i < topTracks.length; i++) {

        //Creating row
        var tr = document.createElement("tr");
        tr.className = "toptrack"
        topTracksChart.appendChild(tr);

        var tdName = document.createElement("td");
        var linkTrack = document.createElement("a");
        linkTrack.setAttribute("href", topTracks[i].URL);
        linkTrack.innerHTML = topTracks[i].name;
        tdName.appendChild(linkTrack);
        tdName.className = "trackName";
        tr.appendChild(tdName);

        var tdArtist = document.createElement("td");
        var linkArtist = document.createElement("a");
        linkArtist.setAttribute("href", topTracks[i].artistURL);
        linkArtist.innerHTML = topTracks[i].artistName;
        tdArtist.appendChild(linkArtist);
        tdArtist.className = "trackArtist";
        tr.appendChild(tdArtist);

        var tdListeners = document.createElement("td");
        tdListeners.innerHTML = topTracks[i].listeners;
        tdListeners.className = "trackListeners";
        tr.appendChild(tdListeners);

        var tdCount = document.createElement("td");
        tdCount.innerHTML = topTracks[i].playcount;
        tdListeners.className = "trackPlaycount";
        tr.appendChild(tdCount);
    }
}
/************************ END ****************************/

/*********************** EVENTS **************************/

function setupEvents() {
    requestData("_assets/data/events.xml", loadEvents);
}

function loadEvents(xmlhttp) {

    var xmlDoc = xmlhttp.responseXML;
    var xmlItems = xmlDoc.getElementsByTagName("event");
    //console.log(xmlItems);

    for (var i = 0; i < xmlItems.length; i++) {

        var eventTitle = xmlItems[i].getElementsByTagName("title")[0].childNodes[0].data;
        var eventHeadliner = xmlItems[i].getElementsByTagName("headliner")[0].childNodes[0].data;
        var eventArtists = xmlItems[i].getElementsByTagName("artist");
        var eventDate = xmlItems[i].getElementsByTagName("startDate")[0].childNodes[0].data;
        var eventURL = xmlItems[i].getElementsByTagName("url")[0].childNodes[0].data;;
        var eventTags = xmlItems[i].getElementsByTagName("tag");

        var Event = {
            title: eventTitle,
            headliner: eventHeadliner,
            artists: eventArtists,
            date: eventDate,
            URL: eventURL,
            tags: eventTags
        };

        events.push(Event);
    }
    /*
    for(var i = 0; i < events.length; i++) { 
    	console.log(events[i]);
    }*/
}

function fillEventsChart() {

    var eventsChart = document.getElementById("charts");

    while (eventsChart.childNodes.length > 0) {
        eventsChart.removeChild(eventsChart.firstChild);
    }

    for (var i = 0; i < events.length; i++) {

        var tr = document.createElement("tr");
        tr.className = "event";
        eventsChart.appendChild(tr);

        var divTitle = document.createElement("div");
        divTitle.innerHTML = events[i].title;
        divTitle.className = "eventTitle";
        tr.appendChild(divTitle);

        var divContents = document.createElement("div");
        divContents.className = "eventContents";
        tr.appendChild(divContents);

        var headliner = document.createElement("div");
        headliner.innerHTML = events[i].headliner;
        headliner.className = "headliner";
        divContents.appendChild(headliner);

        if (events[i].artists.length > 1) {
            var divHeadingArtist = document.createElement("div");
            divHeadingArtist.className = "artistHeading";
            divHeadingArtist.innerHTML = "Other Artist(s) - <br />";
            divContents.appendChild(divHeadingArtist);


            for (var a = 0; a < events[i].artists.length; a++) {

                if (events[i].artists[a].childNodes[0].data != events[i].headliner) {
                    var artist = document.createElement("div");
                    artist.innerHTML = events[i].artists[a].childNodes[0].data;
                    artist.className = "eventArtist";
                    divContents.appendChild(artist);
                }
            }
        }
        var date = document.createElement("div");
        date.innerHTML = events[i].date;
        date.className = "eventDate";
        divContents.appendChild(date);

        var URL = document.createElement("div");
        var link = document.createElement("a");
        link.setAttribute("href", events[i].URL);
        link.innerHTML = "Click here for Venu details";
        URL.appendChild(link);
        URL.className = "eventURL";
        divContents.appendChild(URL);

        for (var t = 1; t < events[i].tags.length; t++) {

            var tag = document.createElement("div");
            tag.innerHTML = events[i].tags[t].firstChild.data;
            tag.className = "eventTag";
            divContents.appendChild(tag);
        }
    }
}
/************************ END ****************************/

function sortByTracks() {
    topTracks.sort(function (a, b) {
        if (a.name < b.name) return -1;
        else return 1;
    });
    fillTopTracksChart();
}

function sortByArtists() {
    topTracks.sort(function (a, b) {
        if (a.artistName < b.artistName) return -1;
        else return 1;
    });
    fillTopTracksChart();
}

function sortByListeners() {

    topTracks.sort(function (a, b) {
        if (a.listeners > b.listeners) return -1;
        else return 1;
    });
    fillTopTracksChart();

}

function sortByPlayCount() {

    topTracks.sort(function (a, b) {
        if (a.playcount > b.playcount) return -1;
        else return 1;
    });
    fillTopTracksChart();

}

function setupSearch() {

    var nav = document.getElementById("navigation");
    var form = document.createElement("form");
    form.id = "search";
    nav.appendChild(form);

    var inputQuery = document.createElement("input");
    inputQuery.setAttribute("type", "text");
    inputQuery.setAttribute("name", "query");
    inputQuery.setAttribute("placeholder", "artists, tracks, events...");
    form.appendChild(inputQuery);

    var searchButton = document.createElement("div");
    searchButton.innerHTML = "Search";
    searchButton.id = "searchButtonInactive";
    searchButton.onclick = function () {
        searchData();
    };
    form.appendChild(searchButton);
}

function searchData() {


    var form = document.getElementById("search");
    var query = form.elements[0].value.toLowerCase();

    if (query.length >= 2) {
        searchArtists(query);
        searchTracks(query);
        searchEvents(query);
    } else if (query.length == 0) {
        alert("empty search query");
    } else {
        alert("Search should be a proper word")
    }

    fillSearchPage();
}

function searchArtists(query) {

    artistSearch.length = 0;

    for (var i = 0; i < topArtists.length; i++) {

        var name = topArtists[i].name.toLowerCase();
        if (name.search(query) >= 0) {
            artistSearch.push(topArtists[i]);
        }
    }
    /*
    for(var i = 0; i < artistSearch.length; i++) {
    	console.log(artistSearch[i]);
    }*/
}

function searchTracks(query) {

    trackSearch.length = 0;

    for (var i = 0; i < topTracks.length; i++) {

        var track = topTracks[i].name.toLowerCase();
        var artist = topTracks[i].artistName.toLowerCase();

        if (track.search(query) >= 0 ||
            artist.search(query) >= 0) {
            trackSearch.push(topTracks[i]);
        }
    }
    /*
    for(var i = 0; i < trackSearch.length; i++) {
    	console.log(trackSearch[i]);
    }*/
}

function searchEvents(query) {

    eventSearch.length = 0;

    for (var i = 0; i < events.length; i++) {
        var title = events[i].title.toLowerCase();

        if (title.search(query) >= 0 ||
            searchEventArtists(query, i) >= 0 ||
            searchEventTags(query, i) >= 0) {
            eventSearch.push(events[i]);
        }
    }
    /*
    for(var i = 0; i < eventSearch.length; i++) {
    	console.log(eventSearch[i]);
    }*/
}

function searchEventArtists(query, index) {

    for (var a = 0; a < events[index].artists.length; a++) {
        var artist = events[index].artists[a].childNodes[0].data.toLowerCase();
        if (artist.search(query) >= 0) {
            return artist.search(query);
        }
    }
}

function searchEventTags(query, index) {

    for (var t = 1; t < events[index].tags.length; t++) {
        var tag = events[index].tags[t].firstChild.data.toLowerCase();
        if (tag.search(query) >= 0) {
            return tag.search(query);
        }
    }
}

function fillSearchPage() {

    var searchResults = document.getElementById("charts");

    while (searchResults.childNodes.length > 0) {
        searchResults.removeChild(searchResults.firstChild);
    }

    if (artistSearch.length == 0 &&
        trackSearch.length == 0 &&
        eventSearch.length == 0) {
        var noresult = document.createElement("tr");
        noresult.id = "noresult";
        noresult.innerHTML = "No Result Found.";
    } else {

        if (artistSearch.length > 0) {
            var tr = document.createElement("tr");
            tr.className = "searchHeading";

            var heading = document.createElement("th");
            heading.innerHTML = "Artist(s)";
            tr.appendChild(heading);
            searchResults.appendChild(tr);

            var artistsResult = document.createElement("tr");
            artistsResult.id = "artistsResult";
            searchResults.appendChild(artistsResult);

            for (var i = 0; i < artistSearch.length; i++) {

                var divArtist = document.createElement("div");
                divArtist.className = "searchedArstist"
                artistsResult.appendChild(divArtist);

                var divImg = document.createElement("div");
                divImg.className = "artistImage";
                var img = document.createElement("img");
                img.setAttribute("src", artistSearch[i].img);
                divImg.appendChild(img);
                divArtist.appendChild(divImg);

                var divName = document.createElement("div");
                divName.className = "artistName";
                var link = document.createElement("a");
                link.setAttribute("href", artistSearch[i].URL);
                link.innerHTML = artistSearch[i].name;
                divName.appendChild(link);
                divArtist.appendChild(divName);
            }
        }

        if (trackSearch.length > 0) {

            var tr = document.createElement("tr");
            tr.className = "searchHeading";
            var heading = document.createElement("th");
            heading.innerHTML = "Track(s)";
            tr.appendChild(heading);
            searchResults.appendChild(tr);

            var tracksResult = document.createElement("tr");
            tracksResult.id = "tracksResult";
            searchResults.appendChild(tracksResult);

            for (var i = 0; i < trackSearch.length; i++) {

                var tdName = document.createElement("td");
                var linkTrack = document.createElement("a");
                linkTrack.setAttribute("href", trackSearch[i].URL);
                linkTrack.innerHTML = trackSearch[i].name;
                tdName.appendChild(linkTrack);
                tdName.className = "trackName";
                tracksResult.appendChild(tdName);

                var tdArtist = document.createElement("td");
                var linkArtist = document.createElement("a");
                linkArtist.setAttribute("href", trackSearch[i].artistURL);
                linkArtist.innerHTML = trackSearch[i].artistName;
                tdArtist.appendChild(linkArtist);
                tdArtist.className = "trackArtist";
                tracksResult.appendChild(tdArtist);
            }
        }

        if (eventSearch.length > 0) {

            var tr = document.createElement("tr");
            tr.className = "searchHeading";
            var heading = document.createElement("th");
            heading.innerHTML = "Event(s)";
            tr.appendChild(heading);
            searchResults.appendChild(tr);

            var eventsResult = document.createElement("tr");
            eventsResult.id = "eventsResult";
            searchResults.appendChild(eventsResult);

            for (var i = 0; i < eventSearch.length; i++) {

                var divTitle = document.createElement("div");
                divTitle.innerHTML = eventSearch[i].title;
                divTitle.className = "eventTitle";
                eventsResult.appendChild(divTitle);

                var divContents = document.createElement("div");
                divContents.className = "eventContents";
                eventsResult.appendChild(divContents);

                var headliner = document.createElement("div");
                headliner.innerHTML = eventSearch[i].headliner;
                headliner.className = "headliner";
                divContents.appendChild(headliner);

                if (events[i].artists.length > 1) {
                    var divHeadingArtist = document.createElement("div");
                    divHeadingArtist.className = "artistHeading";
                    divHeadingArtist.innerHTML = "Other Artist(s) - <br />";
                    divContents.appendChild(divHeadingArtist);


                    for (var a = 0; a < events[i].artists.length; a++) {

                        if (events[i].artists[a].childNodes[0].data != events[i].headliner) {
                            var artist = document.createElement("div");
                            artist.innerHTML = events[i].artists[a].childNodes[0].data;
                            artist.className = "eventArtist";
                            divContents.appendChild(artist);
                        }
                    }
                }

                var date = document.createElement("div");
                date.innerHTML = eventSearch[i].date;
                date.className = "eventDate";
                divContents.appendChild(date);

                var URL = document.createElement("div");
                var link = document.createElement("a");
                link.setAttribute("href", eventSearch[i].URL);
                link.innerHTML = "Click here for Venu details";
                URL.appendChild(link);
                URL.className = "eventURL";
                divContents.appendChild(URL);

                for (var t = 1; t < eventSearch[i].tags.length; t++) {

                    var tag = document.createElement("div");
                    tag.innerHTML = eventSearch[i].tags[t].firstChild.data;
                    tag.className = "eventTag";
                    divContents.appendChild(tag);
                }
            }
        }
    }
}
